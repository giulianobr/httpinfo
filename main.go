package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	// Define HTTP server.
	http.HandleFunc("/", helloRunHandler)

	// PORT environment variable is provided by Cloud Run.
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	log.Print("Hello from GCP! The container started successfully and is listening for HTTP requests on $PORT")
	log.Printf("Listening on port %s", port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal(err)
	}
}

// helloRunHandler responds to requests by rendering an HTML page.
func helloRunHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// Initialize template parameters.
	service := os.Getenv("K_SERVICE")
	if service == "" {
		service = "???"
	}

	revision := os.Getenv("K_REVISION")
	if revision == "" {
		revision = "???"
	}
	Header := r.Header
	for key, value := range Header {
		fmt.Printf("%s: %s", key, value)
	}
	Header.Set("Revision", revision)
	Header.Set("Service", service)
	ne := json.NewEncoder(w)
	ne.SetIndent("", "  ")
	err := ne.Encode(Header)
	if err != nil {
		log.Fatal(err)
	}
}
