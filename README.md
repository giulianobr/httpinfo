# HTTP Info

It is like an HTTPBin endpoint, but simpler.

## Run locally

```
go run main.go
```

## Deploy App Engine

```
gcloud config set project YOUR_PROJECT
gcloud app deploy .
```
